// Copyright 2016, Google, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// [START app]
'use strict';

process.env.DEBUG = 'actions-on-google:*';

let Assistant = require('actions-on-google');
let express = require('express');
let bodyParser = require('body-parser');
let actions = require('./actions');
let INTENTS = require('./actions/intents');


// HTTP Cloud Function handler
exports.thinktank_test = function (request, response) {
    
    console.log('headers: ' + JSON.stringify(request.headers));
    console.log('body: ' + JSON.stringify(request.body));

    const assistant = new Assistant.ActionsSdkAssistant({
        request: request,
        response: response
    });
    let actionMap = new Map();
    Object.keys(INTENTS.intents).map((key) => {
        actionMap.set(INTENTS.intents[key], actions[INTENTS.actions[key]]);
    })
    
    assistant.handleRequest(actionMap)
};