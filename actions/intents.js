exports.id = 'intents'

const intentToActionMap = {
    UNKNOWN_DEEPLINK_ACTION: 'deeplink.unknown',
    DEFAULT_FALLBACK_ACTION: 'input.unknown',
    START_GAME_ACTION: 'start_game',
    GENERATE_ANSWER_ACTION: 'generate_answer',
    CHECK_GUESS_ACTION: 'check_guess',
    CHECK_GUESS_NO_INPUT_ACTION: 'check_guess_no_input',
    PLAY_AGAIN_YES_ACTION: 'play_again_yes',
    PLAY_AGAIN_NO_ACTION: 'play_again_no',
    PLAY_AGAIN_NO_INPUT_ACTION: 'play_again_no_input',
    QUIT_ACTION: 'quit'
};

const data = {
    intents: intentToActionMap,
    actions: {
        UNKNOWN_DEEPLINK_ACTION: 'unhandledDeepLinks',
        DEFAULT_FALLBACK_ACTION: 'defaultFallback',
        START_GAME_ACTION: 'startGame',
        GENERATE_ANSWER_ACTION: 'generateAnswer',
        CHECK_GUESS_ACTION: 'checkGuess',
        PLAY_AGAIN_YES_ACTION: 'playAgainYes',
        PLAY_AGAIN_NO_ACTION: 'playAgainNo',
        QUIT_ACTION: 'quit'
    }
};

module.exports = data