exports.id = 'action_utils'

const animals = require('../animals');
const sprintf = require('sprintf-js').sprintf;

const utils = {
    /**
     * Get a random animal instead of sequentially getting an animal
     * Return -1 if we ahve no more animals to end the game
     */
    getRandomAnimal: function (assistant) {
        assistant.data.previousAnimals = assistant.data.previousAnimals || [];
        if(assistant.data.previousAnimals.length === Object.keys(animals).length) {
            return -1;
        } else {
            //Lets only get non played animals
            let keys = Object.keys(animals).filter((value) => {
                return assistant.data.previousAnimals.indexOf(value) === -1;
            });
            let max = keys.length;
            let min = 0;
            let index = Math.floor(Math.random() * (max));
            assistant.data.previousAnimals.push(keys[index]);
            return animals[keys[index]];
        }
    },
    hasMoreAnimals(assistant) {
        if(assistant.data.previousAnimals.length === Object.keys(animals).length) {
            return false;
        } else {
            return true;
        }
    },
    /**
     * Sequentially get the next animal
     * End the game by returning -1 if we've guessed all animals
     */
    getNextAnimal: function (assistant) {
        assistant.data.previousAnimals = assistant.data.previousAnimals || []
        if(assistant.data.previousAnimals.length === Object.keys(animals).length) {
            return -1;
        } else {
            let keys = Object.keys(animals);
            let index = keys[assistant.data.previousAnimals.length || 0];
            assistant.data.previousAnimals.push(index);
            return animals[index];
        }
    },

    getAnimal: function (assistant) {
        mode = assistant.data.mode || 'linear';
        if(mode === 'random') {
            return this.getRandomAnimal(assistant);
        } else {
            return this.getNextAnimal(assistant);
        }
    },

    /**
     * Utility function to pick prompts
     */ 
    getRandomPrompt: function (assistant, array, args) {
        let lastPrompt = assistant.data.lastPrompt;
        let prompt;
        if (lastPrompt) {
            for (let index in array) {
                prompt = array[index];
                if (prompt != lastPrompt) {
                    break;
                }
            }
        } else if (array && array.constructor == Array) {
            prompt = array[Math.floor(Math.random() * (array.length))];
        } else {
            prompt = array;
        }
        if(args) {
            prompt = this.sprintf(prompt, args);
        }
        return prompt;
    },

    getAnswerString(answers) {
        if(answers.length == 1) {
            return answers[0];
        } else {
            let orAnswer = answers.pop();
            let commaList = answers;
            let string = commaList.join(', ') + ' or '+orAnswer;
            return string;
        }
    },
    speak (message) {
        return '<speak>'+message+'</speak>';
    },

    sprintf (message, variables) {
        return sprintf.apply(this, arguments);
    },

    /**
     * Set the animal audio for the message
     */
    setSound (message, data) {
        if(message && message.constructor == Array) {
            message = this.sprintf(message[0], message[1]);
        }
        if(typeof data === 'string') {
            data = [{
                sound: data,
                playSound: 'after'
            }];
        } else {
            data = data.constructor === Array ? data : [data];
        }
        let messages = [message];
        data.forEach((value, index) => {
            if(value.sound) {
                let sound = "<audio src='"+value.sound+"'></audio>";
                if(value.playSound == 'after') {
                    messages.push(sound);
                } else {
                    messages.unshift(sound);
                }
                //Fatten the message array here so that we don't have an extra period in front of a sound
                messages = [messages.join(' ')];
            }
        })
        return messages.join('. ');
    }
}

module.exports = utils