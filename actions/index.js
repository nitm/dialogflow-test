exports.id = 'actions'
const utils = require('./utils');
const intents = require('./intents');
const messages = require('../messages');
const contexts = require('../contexts');

const Actions = function () {};

Actions.prototype.startGame = function (assistant) {
    console.log('startGame');
    assistant.setContext(contexts.GAME);
    let question = actions.generateAnswer(assistant, true);
    let message = utils.setSound(messages.WELCOME, {
        sound: 'https://thinktank.thinklabserver.com/media/Intro.mp3',
        playSound: 'before'
    });
    console.log(question);
    assistant.ask(utils.speak(message+' '+question));
};

Actions.prototype.generateAnswer = function (assistant, returnQuestion, isPlayAgain) {
    console.log('generateAnswer');
    assistant.setContext(contexts.GAME);
    let answer = actions.getAnswer(assistant);
    let message;
    if(answer && answer !== -1) {   
        if(isPlayAgain) {
            return utils.getRandomPrompt(assistant, messages.PLAY_AGAIN_YES, answer.hints[0]);
        } else {
            return utils.getRandomPrompt(assistant, messages.GAME_START, answer.hints[0]);
        }
    } else {
        //-1 means the game is done
        console.log('generateAnswer: no more animals');
        return false;
    }
    if(!returnQuestion) {
        assistant.ask(utils.speak(message));
    } else {
        return message;
    }
};

Actions.prototype.checkGuess = function (assistant) {
    console.log('checkGuess');
    assistant.data.guesses = assistant.data.guesses || 0;
    let question = assistant.data.answer;
    let guess = assistant.getArgument('guess').toLowerCase();
    let matched = false;
    let answer = null;
    for(var index in question.answers) {
        answer = question.answers[index];
        if(guess.indexOf(answer.toLowerCase()) !== -1) {
            matched = true;
            break;
        }
    }

    // If there are still guesses left then prompt the user
    if (!matched && assistant.data.guesses < question.noMatch.length) {
        let matchedData = question.noMatch[assistant.data.guesses];
        console.log(assistant.data);
        let hint = utils.setSound([matchedData.message, guess], matchedData);
        assistant.data.guesses++;
        //Set the game context here so that we don't overried any further contexts
        assistant.setContext(contexts.GAME);
        assistant.ask(utils.speak(hint));
    } else if (matched && !utils.hasMoreAnimals(assistant)) {
        // We've come to the end of the game at this point. Let's quit gracefully
        let successMessage = utils.setSound(question.success[0].message+' It\'s a '+answer,  question.success[0]);
        let message = utils.getRandomPrompt(assistant, messages.GAME_END);
        assistant.setContext(contexts.QUIT);
        assistant.tell(utils.speak(successMessage+'. <break strength=\'medium\'/>'+message));
    } else if (!matched && assistant.data.guesses === question.noMatch.length) {
        // Otherwise let's stop the guessing
        assistant.setContext(contexts.PLAY_AGAIN);
        //Reset the assistant here to prevent restating the answer
        actions.reset(assistant);
        let failMessage = utils.getRandomPrompt(assistant, messages.GUESS_FAIL+' a '+utils.getAnswerString(question.answers));
        failMessage += '. <break strength=\'medium\'/>'+utils.getRandomPrompt(assistant, messages.PLAY_AGAIN_ASK);
        assistant.ask(utils.speak(failMessage));
    } else {
        assistant.setContext(contexts.PLAY_AGAIN);
        actions.reset(assistant);
        let successMessage = utils.setSound(question.success[0].message+' It\'s a '+answer,  question.success[0]);
        successMessage += '. <break strength=\'medium\'/>'+utils.getRandomPrompt(assistant, messages.PLAY_AGAIN_ASK);
        assistant.ask(utils.speak(successMessage));
    }
};

Actions.prototype.playAgainYes = function (assistant) {
    console.log('playAgainYes');
    assistant.setContext(contexts.GAME);
    let question = actions.generateAnswer(assistant, true, true);
    //If there are no more questions then let's end the game
    if(!question || question === -1) {
        let message = utils.getRandomPrompt(assistant, messages.GAME_END);
        assistant.setContext(contexts.QUIT);
        assistant.tell(utils.speak(message));
    } else {
        // let message = utils.getRandomPrompt(assistant, messages.PLAY_AGAIN_YES, question);
        assistant.ask(utils.speak(question));
    }
};

Actions.prototype.playAgainNo = function (assistant) {
    console.log('playAgainNo');
    assistant.setContext(contexts.QUIT, 1);
    let message = utils.getRandomPrompt(assistant, messages.PLAY_AGAIN_NO);
    assistant.tell(utils.speak(message));
};

Actions.prototype.quit = function (assistant) {
    console.log('quit');
    assistant.setContext(contexts.QUIT);
    let answer = assistant.data.answer;
    let message = utils.getRandomPrompt(assistant, messages.QUIT, answer);
    assistant.tell(utils.speak(message));
};

/**
 * Reset the game with a new answer
 */
Actions.prototype.getAnswer = function (assistant, data) {
    let answer = utils.getAnimal(assistant);
    assistant.data = Object.assign(assistant.data || {}, {
        guesses: 0,
        answer: answer,
        fallbackCount: 0
    }, data || {});
    return answer;
}

/**
 * Reset the game without generating an aswer
 */
Actions.prototype.reset = function (assistant, data) {
    assistant.data = Object.assign(assistant.data || {}, {
        guesses: 0,
        answer: {},
        fallbackCount: 0
    }, data || {});
    return true;
}


Actions.prototype.defaultFallback = function (assistant) {
    console.log('defaultFallback: ' + assistant.data.fallbackCount);
    if (assistant.data.fallbackCount === undefined) {
      assistant.data.fallbackCount = 0;
    }
    assistant.data.fallbackCount++;
    // Provide two prompts before ending game
    if (assistant.data.fallbackCount === 1) {
      assistant.setContext(contexts.PLAY_AGAIN);
      assistant.ask(utils.getRandomPrompt(assistant, messages.FALLBACKS));
    } else {
      assistant.tell(utils.getRandomPrompt(assistant, messages.FALLBACKS));
    }
  }

  Actions.prototype.unhandledDeeplinks = function(assistant) {
    console.log('unhandledDeeplinks');
    let answer = actions.generateAnswer(assistant, true);
    assistant.setContext(contexts.GAME, 1);
    let guess = assistant.getArgument('guess');
    if (guess) {
        return actions.checkGuess(assistant);
    } else {
        actions.defaultFallback(assistant);
    }
  }

const actions = new Actions();

module.exports = actions;