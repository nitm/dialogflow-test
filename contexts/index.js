exports.id = 'contexts';

const contexts = {
    PLAY_AGAIN: 'play_again',
    GAME: 'game',
    QUIT: 'quit'
};

module.exports = contexts