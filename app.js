'use strict';

// Enable actions client library debugging
process.env.DEBUG = 'actions-on-google:*';

let Assistant = require('actions-on-google');
let express = require('express');
let bodyParser = require('body-parser');
let actions = require('./actions');
let INTENTS = require('./actions/intents');

let app = express();
app.set('port', (process.env.PORT || 8080));
app.use(bodyParser.json({
    type: 'application/json'
}));

app.post('/', function (request, response) {
    console.log('headers: ' + JSON.stringify(request.headers));
    console.log('body: ' + JSON.stringify(request.body));

    const assistant = new Assistant.ApiAiAssistant({
        request: request,
        response: response
    });
    let actionMap = new Map();
    Object.keys(INTENTS.intents).map((key) => {
        actionMap.set(INTENTS.intents[key], actions[INTENTS.actions[key]]);
    })
    
    assistant.handleRequest(actionMap)
});

// Start the server
var server = app.listen(app.get('port'), function () {
    console.log('App listening on port %s', server.address().port);
    console.log('Press Ctrl+C to quit.');
});