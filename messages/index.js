exports.id = 'messages';

const messages = {
    WELCOME: ['Okay. Great!'],
    GAME_START: ['<break strength=\'medium\'/>I’ll give you some clues to start with and you try to guess the farm animal I’m thinking of. <break strength=\'medium\'/>Here’s the first one! %s'],
    GAME_END: ['That’s all the animals I have for you now, let’s play again soon!'],
    GUESS_NEXT: ['Nope, it\s not a %s. What\'s your next guess?'],
    GUESS_FAIL: ['Oops. I have no more clues. I was thinking of'],
    GUESS_CORRECT: ['Congratulations, that\'s it! I was thinking of %s'],
    GUESS_NO_INPUT: [
        'Sorry, I didn’t hear you. Do you have a guess?', 
        'Sorry, I still didn’t hear you. Go on, take a guess'
    ],
    PLAY_AGAIN_ASK: [
        'Do you want to play again?', 
        'Would you like to keep playing?',
        'Do you want to guess another animal?',
        'Do you want to play some more?'
    ],
    PLAY_AGAIN_NO_INPUT: [
        'I didn’t get that.  Do you want to play another round of Guess the Farm Animal?',
        'Sorry, I didn’t hear you. Do you want to keep playing?'
    ],
    PLAY_AGAIN_YES: ['Okay Great! <break strength=\'medium\'/> %s'],
    PLAY_AGAIN_NO: ['No problem!  Let’s play again soon!'],
    QUIT: ['Okay. I was thinking of %s. <break strength=\'medium\'/> See you later.'],
    FALLBACKS: [
        "I didn't get that. Can you say it again?",
        "I missed what you said. Say it again?",
        "Sorry, could you say that again?",
        "Sorry, can you say that again?",
        "Sorry, I didn't get that",
        "Sorry, what was that?"
    ]
};

module.exports = messages