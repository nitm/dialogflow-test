exports.id = 'animal-pig';

const animal = {
    answers: ['pig', 'hog', 'piglet'],
    hints: [
        'This animal has four legs and a curly tail',
    ],
    noMatch: [
        {
            message: 'Nope...guess again! They also like to roll around in the mud!'
        }, {
            message: 'Try one last guess. This animal says',
            sound: 'https://thinktank.thinklabserver.com/media/no-match/pignm.mp3',
            playSound: 'after'
        }
    ],
    success: [
        {
            message: 'That\'s right!',
            sound: 'https://thinktank.thinklabserver.com/media/match/pigcallexit.mp3',
            playSound: 'before' 
        }
    ]
};

module.exports = animal