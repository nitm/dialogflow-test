exports.id = 'animal-pig';

const animal = {
    answers: ['chicken', 'hen'],
    hints: [
        'This animal has two legs and lays eggs in a coop.',
    ],
    noMatch: [
        {
            message: 'Nope...guess again! This animal is covered in feathers'
        }, {
            message: 'Try one last guess. This animal says',
            sound: 'https://thinktank.thinklabserver.com/media/no-match/chickennm.mp3',
            playSound: 'after'
        }
    ],
    success: [
        {
            message: 'Fantastic!',
            sound: 'https://thinktank.thinklabserver.com/media/match/chickenexit.mp3',
            playSound: 'before' 
        }
    ]
};

module.exports = animal