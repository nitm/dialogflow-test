exports.id = 'animal-pig';

const animal = {
    answers: ['horse', 'pony', 'stallion', 'mare'],
    hints: [
        'This animal has four legs and a person can ride on it on a saddle.',
    ],
    noMatch: [
        {
            message: 'Nope...guess again!  This animal loves to eat carrots and apples as a treat.'
        }, {
            message: 'Try one last guess. This animal says',
            sound: 'https://thinktank.thinklabserver.com/media/no-match/horsenm.mp3',
            playSound: 'after'
        }
    ],
    success: [
        {
            message: 'Great!',
            sound: 'https://thinktank.thinklabserver.com/media/match/horseexit.mp3',
            playSound: 'before' 
        }
    ]
};

module.exports = animal