exports.id = 'animal-pig';

const animal = {
    answers: ['cow'],
    hints: [
        'This animal eats grass and has udders.',
    ],
    noMatch: [
        {
            message: 'Nope...guess again! This animal sleeps standing up!'
        }, {
            message: 'Try one last guess. This is what this animal sounds like',
            sound: 'https://thinktank.thinklabserver.com/media/no-match/cownm.mp3',
            playSound: 'after'
        }
    ],
    success: [
        {
            message: 'Well done!',
            sound: 'https://thinktank.thinklabserver.com/media/match/cowexit.mp3',
            playSound: 'before' 
        }
    ]
};

module.exports = animal