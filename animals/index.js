
/** Animals */

exports.id = 'animals';

const animals = {
    chicken: require('./chicken'),
    cow: require('./cow'),
    duck: require('./duck'),
    horse: require('./horse'),
    pig: require('./pig')
};

module.exports = animals;