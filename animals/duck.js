exports.id = 'animal-pig';

const animal = {
    answers: ['duck'],
    hints: [
        'This animal is covered in feathers and swims in a pond.',
    ],
    noMatch: [
        {
            message: 'Nope...guess again!  Their babies are called ducklings.'
        }, {
            message: 'Try one last guess. This animal says',
            sound: 'https://thinktank.thinklabserver.com/media/no-match/ducknm.mp3',
            playSound: 'after'
        }
    ],
    success: [
        {
            message: 'Correct!',
            sound: 'https://thinktank.thinklabserver.com/media/match/duckexit.mp3',
            playSound: 'before' 
        }
    ]
};

module.exports = animal